/* INSTRUCTIONS:
 *
 * - Install on both monitoring client and server.
 * - Copy this file to `user.config.js` on both client and server.
 * - Edit `user.config.js` to suite your preferences.
 */
module.exports = {

    /* There are no real rules surrounding this name, but it's what will be
     * shared with you in the emails you receive. You should set this name per
     * client so you can tell at a glance which ones are failing. */
    name: 'unique_name',

    /* Connection information. Server and the Client settings should match. */
    address: 'localhost', /* Exclude 'http' and 'https' prefix. */
    port: 3000,

    /* SSL settings. This is optional, but helps prevent MITM attacks on your
     * server. If set to true, you must provide a path for the certificate. */
    ssl : false,

    /* ONLY set this path on your server.
     * DO NOT INCLUDE YOUR CERTIFICATE IN YOUR CLIENT INSTALLATIONS! */
    certificate_path: '',

    /* --------- CLIENT SETTINGS ------------ */

    ping_duration: 5, //7 * 60, /* time between pings (in seconds) */

    /* TODO: email information (address, credentials, etc...) */
    // IMAP details
};

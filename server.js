/* Config fallbacks. If a new property is added in future updates and the
 * installer forgets to update, nothing should break. */
var config = (function () {
    let base = Object.assign({}, require('./example.config.js'));

    try {
        let config = require('./user.config.js');
        return Object.assign(base, config);
    } catch (err) {
        return base;
    }
}());

const http = config.ssl ? require('https') : require('http');
const server = http.createServer().listen(config.port);

server.on('request', async (req, res) => {
    if (req.method === 'POST' &&
        req.url === '/bleep') {

        /* Allow custom pings to ensure no caching is happening */
        let body = await new Promise((resolve) => {
            let body = '';
            req.on('data', (data) => { body += data; });
            req.on('end', () => { resolve(body); });
        });

        res.writeHead(200, { 'Content-Type': 'text' });
        res.write(body);
        res.end();
        return;
    }

    /* Fallback */
    res.writeHead(404, { 'Content-Type': 'text' });
    res.write('Content not found');
    res.end();
});

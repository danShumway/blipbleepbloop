/* Config fallbacks. If a new property is added in future updates and the
 * installer forgets to update, nothing should break. */
var config = (function () {
    var base = Object.assign({}, require('./example.config.js'));

    try {
        var config = require('./user.config.js');
        return Object.assign(base, config);
    } catch (err) {
        return base;
    }
}());

const http = config.ssl ? require('https') : require('http');

const MESSAGES = {
    data_mismatch: `The server responded with the wrong data. It may be down, or serving cached responses.`,
    general_error: `Error while attempting to contact server. It may have gone down.`
};

async function ping () {

    var date = (new Date()).toString();
    let options = {
        hostname: config.address,
        port: config.port,
        path: '/bleep',
        method: 'POST',
        headers: {
            'Content-Type': 'text',
            'Content-Length': date.length
        }
    };

    /* Actually make the request. */
    return new Promise(async (ping_resolve, ping_error) => {

        const req = http.request(options, async (res) => {

            /* Check to see if the server response matches the request. */
            let body = await new Promise((body_resolve) => {
                let body = '';
                res.on('data', (data) => { body += data; });
                res.on('end', () => { body_resolve(body); });
            });

            if (body !== date) {
                ping_error(new Error(MESSAGES.data_missmatch));
            }

            ping_resolve(date);
        });

        /* Any error for any reason is a problem. */
        req.on('error', (err) => {
            const message =  new Error(MESSAGES.general_error);
            message.immediate_cause = err;
            ping_error(message);
        });

        req.write(date);
        req.end();
    });
}

async function sendEmail (disconnected, error) {
    const client = config.name;

    let subject = disconnected ?
        `Client ${client} disconnect!` :
        `Client ${client} reconnect.`;

    let body = (() => {
        let body = disconnected ?
            `Client ${client} is reporting that its remote server is offline.
If you have other clients connected, you should check their statuses as well.`:
            `Client ${client} is reporting that its remote server is back online.
If you have other clients connected, you should check their statuses as well.` ;

        if (error) {
            body += `\n\n${error.toString()}`;
        }

        if (error && error.immediate_cause) {
            body += `\n\nImmediate Node Error (for debugging):\n${error.immediate_cause.toString()}`;
        }

        return body;
    })();

    //Actual email request here.
    console.log(body);
}

/* Entry point and final logic. */
let disconnected = false; /* We only send emails when the state *changes* */
(async function main () {

    const error = await ping().then(
        () => { return null; },
        (err) => { return Promise.resolve(err); });

    /* The presence or lack of error indicates whether or not the last request
     * was successful. */
    if (disconnected !== !!error) {
        disconnected = !!error;

        sendEmail(disconnected, error);
    }

    /* There's no reason for you to ever have a ping_duration of less than a
       second, so I'm just removing the option. */
    setTimeout(()=>main(), 1000 * config.ping_duration);

}());

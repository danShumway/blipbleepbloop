# BlipBleepBloop

BlipBleepBloop is a simple, small, dependency-free Node utility for monitoring
server health. The codebase is small enough and clear enough that you should
be able to audit and extend the source code yourself. Usage is simple enough
that even inexperienced users can run it.

BlipBleepBloop includes a server and client decision. The client can be run on
multiple computers simultaneously. If *any* of the clients can't connect to the
server, they'll try to send you an email.

This means that if you install the client on multiple computers, an outage might
result in multiple emails being sent. This is intentional -- knowing *which*
computers on *which* networks can't connect will help narrow down potential
issues.

Naturally, if your client goes down or doesn't have an internet connection, you
won't get an email. This is another reason why installing on multiple clients
is a good idea -- it adds extra resiliency to your setup.

# Installation

## Node

You'll need a recent version of Node that supports ``async`` and ``await``.

Unless you're running a slow-release distribution of Linux like CentOS, whatever
version of Node is in your official repos will likely work out of the box.

If you don't feel comfortable setting up Node yourself, NVM is highly recommended.

## Cloning

Either clone (``git clone https://gitlab.com/affablyevil/blipbleepbloop``) or
download the repository to your target computers and open a terminal.

Run the server with ``node server.js``. Run the client with ``node client.js``.

## Running as a Background Service

*TODO: Expand this section.*

# Configuration

Copy ``example.config.js`` to ``user.config.js``. This file will contain all of
the settings for both the client and the server. You *can* edit
``example.config.js`` directly, but if you cloned the repository using Git
then updates will remove any changes you make to this file.

# Updating

If you cloned the repository with Git:

``git add . && git reset --hard && git pull``

If you manually downloaded, just download the new release again and replace your
existing installation folder.

Regardless of how you update, you'll want to take a look at your config and
update with any new settings that have been added.

# Support

If you find BlipBleepBloop useful and you'd like me to build more software like
it, consider [donating](https://patreon.com/danshumway). It'll encourage me to
spend more time on Open Source development.
